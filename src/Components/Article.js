import React, {Component} from 'react'
import CommentList from './CommentList'

export default class Article extends Component {
  render() {
    const {article: {title, text, comments}, isOpen, openArticle} = this.props;
    const body = isOpen ? <article>{text}</article> : null
    const coms = isOpen ? <CommentList comments={comments}/> : null

    return (
      <div>
        <h1 onClick={openArticle}>{title}</h1>
        {body}
        {coms}
      </div>
    )
  }
}
