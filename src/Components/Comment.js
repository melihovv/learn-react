import React, {Component} from 'react'

export default class Comment extends Component {
  render() {
    const {comment: {user, text}} = this.props;

    return (
      <div>
        <h4>{user}</h4>
        <article>{text}</article>
      </div>
    )
  }
}
