import React, {Component} from 'react'
import Comment from './Comment'
import toggleOpen from '../Decorators/toggleOpen'

@toggleOpen
export default class CommentList extends Component {
  render() {
    const {comments, isOpen, toggleOpen} = this.props;
    const coms = isOpen
      ? comments.map(comment => <li key={comment.id}><Comment comment={comment}/></li>)
      : null

    return (
      <div>
        <h3 onClick={toggleOpen}>Comment list</h3>
        <ul>
          {coms}
        </ul>
      </div>
    )
  }
}
