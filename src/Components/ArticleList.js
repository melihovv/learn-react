import React, {Component} from 'react'
import Article from './Article'
import oneOpen from '../Decorators/oneOpen'

@oneOpen
export default class ArticleList extends Component {
  render() {
    const {articles, isItemOpened, toggleItem, openItem} = this.props;

    return (
      <div>
        <h1>Article list</h1>
        <ul>
          {articles.map(article => (
            <li key={article.id}>
              <Article article={article}
                       isOpen={isItemOpened(article.id)}
                       openArticle={toggleItem(article.id)}/>
            </li>
          ))}
        </ul>
      </div>
    )
  }
}
