import React from 'react'

export default (Component) => class OneOpen extends React.Component {
  state = {
    openItemId: null,
  }

  openItem = id => ev => {
    if (ev) {
      ev.preventDefault()
    }

    this.setState({
      openItemId: id,
    })
  }

  toggleItem = id => ev => {
    if (ev) {
      ev.preventDefault()
    }

    this.setState({
      openItemId: id === this.state.openItemId ? null : id,
    })
  }

  isItemOpened = id => this.state.openItemId === id

  render() {
    return <Component {...this.props} isItemOpened={this.isItemOpened}
                      openItem={this.openItem}
                      toggleItem={this.toggleItem}/>
  }
}